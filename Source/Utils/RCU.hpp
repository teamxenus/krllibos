#pragma once

namespace RCU
{
    extern void ReadLock();
    extern void ReadUnlock();
}