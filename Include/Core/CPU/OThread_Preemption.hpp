/*
    Purpose:
    Author: Reece W.
    License: All Rights Reserved J. Reece Wilson
*/
#pragma once

LIBLINUX_SYM void ThreadingNoPreempt();
LIBLINUX_SYM void ThreadingAllowPreempt();